# README
Author: Martin Jeanty - martin@jeanty.dk

FreelanceR is a demo ASP.NET MVC 4 web application with a MS SQL 2012 backend database.
It can be run directly from Visual Studio but requires an internet connection due to the database not being local.

Demousers for the site:
User1: DemoAdmin1
Pass: StrongPassword
Role: Administrator

User2: DemoFreelancer1
Pass: StrongPassword
Role: Freelancer

New users can be created but have to be assigned a role through the administrator account in order to be gain useful access to the site.