﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using FreelancerDemo.Models;
using WebMatrix.WebData;

namespace FreelancerDemo.Controllers
{
    [Authorize(Roles = "Administrator, Freelancer")]
    public class WorkSummaryController : Controller
    {
        private freelancerdbEntities db = new freelancerdbEntities();

        public ActionResult Index()
        {
            return View();
        }

        [HttpPost]
        public ActionResult Index(WorkRecordSummaryModel viewmodel)
        {
            int currentUserId = WebSecurity.CurrentUserId;
            double totalHours = 0.0;
            if (ModelState.IsValid && viewmodel.DateFrom < viewmodel.DateTo)
            {
                var model = new WorkRecordSummaryModel();
                // Fetch all workrecords for the current user and sum up hours per project
                var results = from pr in db.Project
                              join wr in
                                  (
                                      (from wr0 in db.WorkRecord
                                       where
                                           wr0.UserId == currentUserId &&
                                           wr0.Date > viewmodel.DateFrom &&
                                           wr0.Date < viewmodel.DateTo
                                       group new { wr0.Project, wr0 } by new
                                                    {
                                                        wr0.Project.Name
                                                    }
                                           into g
                                           select new
                                                      {
                                                          g.Key.Name,
                                                          ProjectTotalHours = (double?)g.Sum(p => p.wr0.Hours)
                                                      })) on pr.Name equals wr.Name
                              select new
                                         {
                                             pr.Customer.Name,
                                             Project = wr.Name,
                                             wr.ProjectTotalHours
                                         };
                foreach (var result in results)
                {
                    var modelentry = new WorkRecordSummaryEntry();
                    modelentry.Customer = result.Name;
                    modelentry.Project = result.Project;
                    modelentry.Hours = (double)result.ProjectTotalHours;
                    totalHours += modelentry.Hours;
                    model.Entries.Add(modelentry);
                }
                ViewBag.TotalHours = totalHours;
                return View(model);
            }
            return View(viewmodel);
        }
    }
}
