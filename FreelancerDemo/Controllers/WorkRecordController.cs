﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using FreelancerDemo.Models;
using WebMatrix.WebData;

namespace FreelancerDemo.Controllers
{
    [Authorize(Roles = "Administrator, Freelancer")]
    public class WorkRecordController : Controller
    {
        private freelancerdbEntities db = new freelancerdbEntities();

        public ActionResult Index()
        {
            List<WorkRecord> records = db.WorkRecord.OrderBy(x => x.Date).ToList();
            var myrecords = new List<WorkRecord>();
            foreach (var record in records)
            {
                if (record.UserId == WebSecurity.CurrentUserId)
                {
                    myrecords.Add(record);
                }    
            }
            return View(myrecords);
        }

        public ActionResult Create()
        {
            ViewBag.UserId = WebSecurity.CurrentUserId;
            ViewBag.WorkTypeId = new SelectList(db.WorkType, "WorkTypeId", "Type");
            ViewBag.ProjectId = new SelectList(db.Project, "ProjectId", "Name");
            return View();
        }

        [HttpPost]
        public ActionResult Create(WorkRecord record)
        {
            if (ModelState.IsValid)
            {
                db.WorkRecord.Add(record);
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            ViewBag.WorkTypeId = new SelectList(db.WorkType, "WorkTypeId", "Type");
            ViewBag.ProjectId = new SelectList(db.Project, "ProjectId", "Name");
            return View(record);
        }

        public ActionResult Edit(int id = 0)
        {
            WorkRecord workRecord = db.WorkRecord.Find(id);
            ViewBag.WorkTypeId = new SelectList(db.WorkType, "WorkTypeId", "Type");
            ViewBag.ProjectId = new SelectList(db.Project, "ProjectId", "Name");
            if (workRecord == null)
            {
                return HttpNotFound();
            }
            return View(workRecord);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(WorkRecord workRecord)
        {
            if (ModelState.IsValid)
            {
                db.Entry(workRecord).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(workRecord);
        }

        public ActionResult Delete(int id = 0)
        {
            WorkRecord record = db.WorkRecord.Find(id);
            if (record == null)
            {
                return HttpNotFound();
            }
            return View(record);
        }

        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            WorkRecord record = db.WorkRecord.Find(id);
            db.WorkRecord.Remove(record);
            db.SaveChanges();
            return RedirectToAction("Index");
        }
    }
}
