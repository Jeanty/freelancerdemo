﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace FreelancerDemo.Controllers
{
    public class WorkTypeController : Controller
    {
        private freelancerdbEntities db = new freelancerdbEntities();

        public ActionResult Index()
        {
            return View(db.WorkType.ToList());
        }

        public ActionResult Create()
        {
            return View();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(WorkType worktype)
        {
            if (ModelState.IsValid)
            {
                db.WorkType.Add(worktype);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            return View(worktype);
        }

        public ActionResult Edit(int id = 0)
        {
            WorkType worktype = db.WorkType.Find(id);
            if (worktype == null)
            {
                return HttpNotFound();
            }
            return View(worktype);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(WorkType worktype)
        {
            if (ModelState.IsValid)
            {
                db.Entry(worktype).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(worktype);
        }

        public ActionResult Delete(int id = 0)
        {
            WorkType worktype = db.WorkType.Find(id);
            if (worktype == null)
            {
                return HttpNotFound();
            }
            return View(worktype);
        }

        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            WorkType worktype = db.WorkType.Find(id);
            db.WorkType.Remove(worktype);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            db.Dispose();
            base.Dispose(disposing);
        }
    }
}