﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Security;

namespace FreelancerDemo.Controllers
{
    [Authorize(Roles = "Administrator")]
    public class UserController : Controller
    {
        private freelancerdbEntities db = new freelancerdbEntities();

        public ActionResult Index()
        {
            return View(db.User.ToList());
        }

        public ActionResult Edit(int id = 0)
        {
            User user = db.User.Find(id);
            if (user == null)
            {
                return HttpNotFound();
            }
            var roles = Roles.GetAllRoles().ToList();
            foreach (String role in Roles.GetRolesForUser(user.UserName))
            {
                if (roles.Contains(role))
                {
                    roles.Remove(role);
                }
            }
            ViewBag.Roles = new SelectList(roles);
            return View(user);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(User user, String rolename)
        {
            if (ModelState.IsValid)
            {
                db.Entry(user).State = EntityState.Modified;
                if (rolename != null && !Roles.GetRolesForUser().Contains(rolename))
                {
                    Roles.AddUserToRole(user.UserName, rolename);
                }
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(user);
        }

        public ActionResult Delete(int id = 0)
        {
            User user = db.User.Find(id);
            if (user == null)
            {
                return HttpNotFound();
            }
            return View(user);
        }

        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            User user = db.User.Find(id);
            db.User.Remove(user);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            db.Dispose();
            base.Dispose(disposing);
        }
    }
}