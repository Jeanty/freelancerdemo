﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace FreelancerDemo.Models
{
    public class WorkRecordSummaryModel
    {
        [Required(ErrorMessage = "You must select a From date")]
        public DateTime DateFrom { get; set; }
        [Required(ErrorMessage = "You must select a To date")]
        public DateTime DateTo { get; set; }
        public List<WorkRecordSummaryEntry> Entries = new List<WorkRecordSummaryEntry>();
    }

    public class WorkRecordSummaryEntry
    {
        public string Customer;
        public string Project;
        public double Hours;    
    }
}